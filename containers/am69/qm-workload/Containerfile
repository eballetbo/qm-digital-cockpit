ARG BASE_NAME=base
ARG IMAGE_TAG=latest
ARG DOCKER_REGISTRY=localhost

ARG DIGITAL_COCKPIT_USER=digital-cockpit
ARG DIGITAL_COCKPIT_GROUP=digital-cockpit
ARG DIGITAL_COCKPIT_UID=1020
ARG DIGITAL_COCKPIT_GID=1020
ARG DIGITAL_COCKPIT_HOME=/var/lib/digital-cockpit
ARG TI_LIBS_GIT_REPO=git://git.ti.com/graphics/ti-img-rogue-umlibs.git
ARG TI_LIBS_GIT_BRANCH=linuxws/kirkstone/k6.1/23.2.6460340
ARG TI_PLATFORM=j784s4

FROM $DOCKER_REGISTRY/$BASE_NAME:$IMAGE_TAG AS base

ARG DIGITAL_COCKPIT_USER
ARG DIGITAL_COCKPIT_GROUP
ARG DIGITAL_COCKPIT_UID
ARG DIGITAL_COCKPIT_GID
ARG DIGITAL_COCKPIT_HOME
ARG TI_LIBS_GIT_REPO
ARG TI_LIBS_GIT_BRANCH
ARG TI_PLATFORM

RUN dnf -y update                                                       \
    && dnf -y install dnf-plugins-core                                  \
    && dnf copr enable -y eballetbo/automotive                          \
    && dnf -y install 'dnf-command(versionlock)'                        \
    && dnf versionlock exclude mesa-va-drivers                          \
    && dnf versionlock add mesa-*-22.3.5-2.20231109git7c9522a4147.fc39  \
    && dnf -y install git                                               \
                      qt6-qt5compat                                     \
                      qt6-qtwebsockets                                  \
                      qt6-qtbase-devel                                  \
                      qt6-qtbase-private-devel                          \
                      qt6-qtquick3d-devel                               \
                      qt6-qtshadertools-devel                           \
                      qt6-qtdeclarative-devel                           \
                      qt6-qtquick3d                                     \
                      libxkbcommon-devel                                \
                      mesa-libgbm-devel                                 \
                      gtk3-devel                                        \
                      gtk4-devel                                        \
    && git clone --depth 1 -b $TI_LIBS_GIT_BRANCH $TI_LIBS_GIT_REPO     \
           ti_libs                                                      \
    && cd ti_libs/targetfs/${TI_PLATFORM}_linux/lws-generic/            \
    && cp -a release/etc/* /etc                                         \
    && cp -a release/lib/* /lib                                         \
    && cp -a release/usr/* /usr                                         \
    && cd -                                                             \
    && rm -fr ti_libs                                                   \
    && dnf -y remove git                                                \
    && dnf clean all && rm -rf /var/cache/dnf                           \
    && groupadd --gid $DIGITAL_COCKPIT_GID $DIGITAL_COCKPIT_GROUP       \
    && useradd -u $DIGITAL_COCKPIT_UID -g $DIGITAL_COCKPIT_GROUP        \
               -d $DIGITAL_COCKPIT_HOME $DIGITAL_COCKPIT_USER           \
    && mkdir -p $DIGITAL_COCKPIT_HOME                                   \
    && install -m 700 -o $DIGITAL_COCKPIT_USER                          \
                      -g $DIGITAL_COCKPIT_GROUP                         \
                      -d $DIGITAL_COCKPIT_HOME                          \
    && install -m 700 -o $DIGITAL_COCKPIT_USER                          \
                      -g $DIGITAL_COCKPIT_GROUP                         \
                      -d /run/user/$DIGITAL_COCKPIT_UID
