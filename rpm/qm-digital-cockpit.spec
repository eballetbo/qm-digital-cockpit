%define _version 0.0.1
%define _release 1
%define _user digital-cockpit
%define _group digital-cockpit
%define _uid 1020
%define _gid 1020

%global debug_package %{nil}

Name:           qm-digital-cockpit
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary:        qm-digital-cockpit files and services

License:        MIT
URL:            http://fedoraproject.org/
Source:         %{name}-%{version}.tar.xz

BuildRequires:  meson
BuildRequires:  systemd
BuildRequires:  systemd-rpm-macros
BuildArch:      noarch
Requires(pre):  /usr/sbin/useradd, /usr/bin/getent, /usr/bin/mkdir, /usr/bin/chown

%description
This package contains the qm-digital-cockpit assets


%package selinux-policy
Summary:        SELinux policy module for %{name}
BuildRequires:  selinux-policy
BuildRequires:  selinux-policy-devel
BuildRequires:  make
BuildArch:      noarch
%{?selinux_requires}

%description selinux-policy
This package contains the SELinux policy module for %{name}.

%package android-vm
Summary:        Android vm container for %{name}
BuildArch:      noarch

%package session-bus
Summary:        Session bus for %{name}
BuildArch:      noarch

%description session-bus
This package contains the Session bus for %{name}.

%description android-vm
This package contains the Android vm container for %{name}.

%package dms
Summary:        Driver Monitoring System container for %{name}
BuildArch:      noarch

%description dms
This package contains the Driver Monitoring System container for %{name}.

%package instrument-cluster
Summary:        Instrument Cluster  container for %{name}
BuildArch:      noarch

%description instrument-cluster
This package contains the Instrument Cluster container for %{name}.

%package ivi
Summary:        In-vehicle infotainment container for %{name}
BuildArch:      noarch

%description ivi
This package contains the In-vehicle infotainment container for %{name}.

%package firefox
Summary:        Firefox container for %{name}
BuildArch:      noarch

%description firefox
This package contains the Firefox container for %{name}.

%package weston
Summary:        Weston compositor container for %{name}
BuildArch:      noarch

%description weston
This package contains the Weston compositor container for %{name}.

%prep
%autosetup -p1

%build
%meson                 \
  -Ddc_user=%{_user}   \
  -Ddc_group=%{_group} \
  -Ddc_uid=%{_uid}     \
  -Ddc_uid=%{_gid}     \
  %{nil}
%meson_build

%install
%meson_install

%pre
if [ $1 = 1 ]; then
  # Initial installation
  /usr/bin/getent group %{_group} >/dev/null || /usr/sbin/groupadd -g %{_gid} %{_group}
  if ! /usr/bin/getent passwd %{_user} >/dev/null ; then
      /usr/sbin/useradd -u %{_uid} -g %{_group} -m -d %{_sharedstatedir}/%{_user} -c "%{name}" %{_user}
  fi
fi

%post session-bus
%systemd_post dc-session-bus.socket
%systemd_post dc-session-bus.service

%preun session-bus
%systemd_preun dc-session-bus.socket
%systemd_preun dc-session-bus.service

%post selinux-policy
%selinux_modules_install %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2

%postun selinux-policy
if [ $1 -eq 0 ]; then
    %selinux_modules_uninstall %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2
fi

%post weston
%systemd_post weston.socket

%preun weston
%systemd_preun weston.socket

%files
%license LICENSE
%attr(0644,root,root) %{_prefix}/lib/tmpfiles.d/digital-cockpit.conf

%files session-bus
%attr(0644,root,root) %{_prefix}/lib/systemd/system/dc-session-bus.socket
%attr(0644,root,root) %{_prefix}/lib/systemd/system/dc-session-bus.service
%attr(0644,root,root) %{_prefix}/lib/systemd/system-preset/*-dc-session-bus.preset

%files selinux-policy
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_datadir}/selinux/packages/qm-digital-cockpit.pp.bz2
%attr(0644,root,root) %{_datadir}/selinux/devel/include/contrib/qm-digital-cockpit.if

%files android-vm
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/android-vm.container

%files dms
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/dms.container

%files instrument-cluster
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/instrument-cluster.container

%files ivi
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/ivi.container

%files firefox
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/firefox.container

%files weston
%defattr(-,root,root,0755)
%attr(0644,root,root) %{_prefix}/lib/systemd/system/weston.socket
%attr(0644,root,root) %{_sysconfdir}/containers/systemd/weston.container
%attr(0644,root,root) %{_prefix}/lib/systemd/system-preset/*-weston.preset

%changelog
* Fri Nov 10 2023 Roberto Majadas <rmajadas@redhat.com>
-