#!/bin/bash
set -e

if [ "$1"x == "--gitlab-ci"x ];
then
    echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    CONTAINER_BUILDER="docker"
    for CONTAINERFILE in $(find containers/ -name Containerfile);
    do
        cd $(dirname $CONTAINERFILE)
        mv Containerfile Dockerfile
        cd -
    done
else
    CONTAINER_BUILDER="buildah"
fi

REGISTRY="localhost/qm-dc"
BUILD_ARG_ARCH=""
BUILD_ARG_BASE_IMAGE=""

if [ "$1"x == "--gitlab-ci"x ];
then
    if [ "$2"x == "--arm64"x ];
    then
        REGISTRY="$CI_REGISTRY_IMAGE/arm64v8"
        BUILD_ARG_ARCH="--build-arg IMAGE_ARCH=linux/arm64v8"
        BUILD_ARG_BASE_IMAGE="--build-arg IMAGE_TAG=arm64v8/fedora:39"
    else
        REGISTRY="$CI_REGISTRY_IMAGE"
    fi
fi

$CONTAINER_BUILDER build -t $REGISTRY/base $BUILD_ARG_ARCH $BUILD_ARG_BASE_IMAGE containers/base
$CONTAINER_BUILDER build -t $REGISTRY/qm-workload:latest $BUILD_ARG_ARCH --build-arg DOCKER_REGISTRY=$REGISTRY containers/qm-workload
$CONTAINER_BUILDER build -t $REGISTRY/weston:latest $BUILD_ARG_ARCH --build-arg DOCKER_REGISTRY=$REGISTRY containers/weston

if [ "$1"x == "--gitlab-ci"x ];
then
    docker push $REGISTRY/base:latest
    docker push $REGISTRY/qm-workload:latest
    docker push $REGISTRY/weston:latest
fi
