#!/bin/bash
set -e

if [ "$1"x == "--git"x ];
then
    COMMITS_NUM_SINCE_LAST_TAG=$(git log $(git describe --tags --abbrev=0)..HEAD --oneline | wc -l)
    COMMIT_HASH=$(git log -1 --pretty=format:%h)
    export GIT_RELEASE="1.$COMMITS_NUM_SINCE_LAST_TAG.$COMMIT_HASH"
fi

BUILDDIR=$(mktemp -d)

meson setup $BUILDDIR

PACKAGE=$(meson introspect "$BUILDDIR" --projectinfo | jq -r .descriptive_name)
VERSION=$(cat version.txt)

if [ -z ${GIT_RELEASE+x} ];
then
    RELEASE=1
    DIST_TARBALL="$BUILDDIR/meson-dist/$PACKAGE-$VERSION.tar.xz"
else
    RELEASE=$GIT_RELEASE
    DIST_TARBALL="$BUILDDIR/meson-dist/$PACKAGE-$VERSION-$RELEASE.tar.xz"
fi

DIST_TARBALL_FILENAME=$(basename $DIST_TARBALL)

SPEC_FILENAME="$PACKAGE.spec"
SPEC_SRC=$(realpath $(dirname $0)/../rpm/$SPEC_FILENAME)

RPM_DIR=$(rpm --eval %_rpmdir)
SOURCE_DIR=$(rpm --eval %_sourcedir)
SPEC_DIR=$(rpm --eval %_specdir)
SRC_RPM_DIR=$(rpm --eval %_srcrpmdir)
ARCH=$(rpm --eval %_target_cpu)

meson dist -C $BUILDDIR

cp "$DIST_TARBALL" "$SOURCE_DIR/"
cp "$SPEC_SRC" "$SPEC_DIR/"

rm -fr $BUILDDIR

sed -e "s/define _version 0.0.1/define _version $VERSION/g" -i $SPEC_DIR/$SPEC_FILENAME
sed -e "s/define _release 1/define _release $RELEASE/g" -i $SPEC_DIR/$SPEC_FILENAME

if [ ! -z ${GIT_RELEASE+x} ];
then
    sed -e "s/Source:.*$/Source:         %{name}-%{version}-%{_release}.tar.xz/g" -i $SPEC_DIR/$SPEC_FILENAME
    sed -e "s/%autosetup -p1/%autosetup -p1 -n %{name}-%{version}-%{_release}/g" -i $SPEC_DIR/$SPEC_FILENAME
    cat $SPEC_DIR/$SPEC_FILENAME
fi


rpmbuild -bs $SPEC_DIR/$SPEC_FILENAME
cp "$SRC_RPM_DIR"/$PACKAGE-*"$VERSION"*.src.rpm .
