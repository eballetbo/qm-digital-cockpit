#!/bin/bash

if [ -z ${GIT_RELEASE+x} ];
then
    cat version.txt
else
    echo -n "$(cat version.txt)-$GIT_RELEASE"
fi
